#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <pthread.h>
#include <time.h>

#include "nfc_api.h"
#include "linux_nfc_api.h"

#define TIME_UTC 1

static pthread_mutex_t mutex;

static nfcTagCallback_t g_TagCB;
static nfc_tag_info_t g_tagInfos[10];
static int count = 0;

void onTagArrival(nfc_tag_info_t *p){
  fprintf(stderr, "Tag detected: (uid ");
  unsigned int i = 0;
  for(i = 0; i < p->uid_length; i++)
      fprintf(stderr, "%02X ", (unsigned char) p->uid[i]);
  fprintf(stderr, "tech %x, handle %x, protocol %x)", p->technology, p->handle, p->protocol);
  pthread_mutex_lock(&mutex);
  if (count < sizeof(g_tagInfos) / sizeof(g_tagInfos[0]))
    g_tagInfos[count++] = *p;
  else
    fprintf(stderr, " (skipeed)");
  pthread_mutex_unlock(&mutex);
  fprintf(stderr, "\n");
}

void onTagDeparture(void){
  fprintf(stderr, "Tag removed\n");
}

void* nfc_thread(void*) { // may freeze / crash. /vendor/bin/hw/android.hardware.nfc@1.0-service must be running
  nfcManager_doInitialize();
  g_TagCB.onTagArrival = onTagArrival;
  g_TagCB.onTagDeparture = onTagDeparture;
  nfcManager_registerTagCallback(&g_TagCB);
  nfcManager_enableDiscovery(DEFAULT_NFA_TECH_MASK, 0x01, 0, 0);
  return 0;
}

void nfc_init() {
  pthread_mutex_init(&mutex, NULL);
  pthread_t tid;
  pthread_create(&tid, NULL, &nfc_thread, NULL);
}

void nfc_close() {
  nfcManager_doDeinitialize();
  pthread_mutex_destroy(&mutex);
}

int nfc_read(char* buf, int size) {
  pthread_mutex_lock(&mutex);
  if (count <= 0) {
    pthread_mutex_unlock(&mutex);
    return -1;
  }
  nfc_tag_info_t *p = &g_tagInfos[--count];
  size_t i;
  for (i = 0;  i < p->uid_length && i < size; i++)
    sprintf(buf + 2*i, "%02X", p->uid[i]);
  pthread_mutex_unlock(&mutex);
  return i;
}
