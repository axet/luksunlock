LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := luksunlock

LOCAL_SRC_FILES := \
	luksunlock.cpp \
	kbd.cpp \
	security.cpp \
	touch.cpp

RECOVERY_API_VERSION := 2
LOCAL_CFLAGS += -DRECOVERY_API_VERSION=$(RECOVERY_API_VERSION) -Wall
LOCAL_CFLAGS += -Wno-error -Wno-unused-parameter

LOCAL_SHARED_LIBRARIES += libminui libpng libcutils
LOCAL_SHARED_LIBRARIES += libc libz
LOCAL_SHARED_LIBRARIES += libbase
LOCAL_SHARED_LIBRARIES += liblog

LOCAL_REQUIRED_MODULES += cryptsetup

LOCAL_MODULE_PATH := $(TARGET_OUT_OPTIONAL_EXECUTABLES)

ifeq ($(TARGET_USES_NFC), TRUE)
LOCAL_CFLAGS += -DNFC
LOCAL_STATIC_LIBRARIES += luksunlock-nfc
LOCAL_C_INCLUDES += luksunlock/nfc
LOCAL_SHARED_LIBRARIES += libnfc-nci libnfc
endif

ifeq ($(TARGET_USES_NFC), 10)
LOCAL_CFLAGS += -DNFC
LOCAL_STATIC_LIBRARIES += luksunlock-nfc10
LOCAL_C_INCLUDES += luksunlock/nfc10
LOCAL_SHARED_LIBRARIES += libnfc-nci libnfc
endif

include $(BUILD_EXECUTABLE)

include $(call all-makefiles-under,$(LOCAL_PATH))
