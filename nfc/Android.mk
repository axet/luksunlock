LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := luksunlock-nfc

LOCAL_CFLAGS += -DRECOVERY_API_VERSION=$(RECOVERY_API_VERSION) -Wall
LOCAL_CFLAGS += -Wno-error -Wno-unused-parameter

LOCAL_MODULE_PATH := $(TARGET_OUT_OPTIONAL_EXECUTABLES)

LOCAL_CFLAGS += -Wno-unused-variable -Wno-writable-strings -Wno-null-conversion -Wno-sign-compare -Wno-parentheses-equality -Wno-unused-function \
  -Wno-missing-braces \
  -Wno-unused-private-field

LOCAL_SRC_FILES += nfc.cpp \
  linux_nfc_api.c \
  nativeNdef.cpp \
  nativeNfcLlcp.cpp \
  nativeNfcSnep.cpp \
  nativeNfcHandover.cpp \
  nativeNfcManager.cpp \
  nativeNfcTag.cpp \
  NfcTag.cpp \
  Mutex.cpp \
  CondVar.cpp \
  IntervalTimer.cpp \
  phNxpExtns.c \
  RoutingManager.cpp \
  phNxpExtns_MifareStd.c \
  phNxpLog.c

LOCAL_CFLAGS += -DNXP_EXTNS=TRUE

VOB_COMPONENTS := system/nfc
NFA := $(VOB_COMPONENTS)/src/nfa
NFC := $(VOB_COMPONENTS)/src/nfc

LOCAL_C_INCLUDES += \
    frameworks/native/include \
    libnativehelper/include/nativehelper \
    $(NFA)/include \
    $(NFA)/brcm \
    $(NFC)/include \
    $(NFC)/brcm \
    $(NFC)/int \
    $(VOB_COMPONENTS)/src/hal/include \
    $(VOB_COMPONENTS)/src/hal/int \
    $(VOB_COMPONENTS)/src/include \
    $(VOB_COMPONENTS)/src/gki/ulinux \
    $(VOB_COMPONENTS)/src/gki/common \
    $(VOB_COMPONENTS)/halimpl/pn54x/common \
    $(VOB_COMPONENTS)/halimpl/pn54x/utils \
    $(VOB_COMPONENTS)/halimpl/pn54x/hal/

include $(BUILD_STATIC_LIBRARY)
