#include <stdio.h>
#include <stdarg.h>

#include "nfc_api.h"
#include "linux_nfc_api.h"
#include "phNxpLog.h"

const char * NXPLOG_ITEM_EXTNS   = "NxpExtns:   ";
const char * NXPLOG_ITEM_API     = "NxpFunc:    ";

nci_log_level_t gLog_level;

void phNxpLog_LogMsg(UINT32 trace_set_mask, const char *item, const char *fmt_str, ...) {
  fprintf(stderr, "%s", item);
  va_list argptr;
  va_start(argptr, fmt_str);
  vfprintf(stderr, fmt_str, argptr);
  va_end(argptr);
  fprintf(stderr, "\n");
  fflush(stderr);
}

UINT8 NFA_SnepSetTraceLevel (UINT8 new_level) {
  return 0;
}

void gLog_levelSet(int i) {
  gLog_level.global_log_level = i;
  gLog_level.extns_log_level = i;
  gLog_level.hal_log_level = i;
  gLog_level.ncix_log_level = i;
  gLog_level.ncir_log_level = i;
  gLog_level.dnld_log_level = i;
  gLog_level.tml_log_level = i;
  gLog_level.dnld_log_level = i;
  gLog_level.dnld_log_level = i;
}

void phNxpLog_InitializeLogLevel() {
  // gLog_levelSet(NXPLOG_LOG_DEBUG_LOGLEVEL);
}
