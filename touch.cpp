#include <vector>
#include <linux/input.h>

#include "luksunlock.h"
#include "touch.h"

int g_touch[64] = { 0 }; // touch array ixycb
int g_slot = 0;

void touch_down(int slot, int x, int y) {
  for (int i = 0; i < CHARS_COUNT + g_buttons_count; i++) {
    Keymap *k = &g_keys[i];
    if (k->xpos <= x && x <= k->xxpos && k->ypos <= y && y <= k->yypos) {
      g_current = i;
      if (g_touch[g_slot * 5 + 3] == -1)
        g_touch[g_slot * 5 + 3] = i;
    }
  }
  g_touch[g_slot * 5 + 4] = 0;
}

void touch_up(int slot, int x, int y) {
  for (int i = 0; i < CHARS_COUNT + g_buttons_count; i++) {
    Keymap *k = &g_keys[i];
    if (k->xpos <= x && x <= k->xxpos && k->ypos <= y && y <= k->yypos) {
      if (g_touch[g_slot * 5 + 3] == (int) i)
        key_select();
    }
  }
  g_touch[g_slot * 5 + 3] = -1; // reset last current
}

void handle_touch(struct input_event *event) {
  if (event->type == EV_ABS) {
    switch (event->code) {
    case ABS_MT_SLOT:
      g_slot = event->value;
    break;
    case ABS_MT_POSITION_X:
      g_touch[g_slot * 5 + 1] = event->value;
      g_touch[g_slot * 5 + 4] = 1;
    break;
    case ABS_MT_POSITION_Y:
      g_touch[g_slot * 5 + 2] = event->value;
      g_touch[g_slot * 5 + 4] = 1;
    break;
    case ABS_MT_PRESSURE:
    break;
    case ABS_MT_TOUCH_MAJOR:
    break;
    case ABS_MT_TRACKING_ID:
      g_touch[g_slot * 5] = event->value;
      if (g_touch[g_slot * 5] == -1)
        touch_up(g_slot, g_touch[g_slot * 5 + 1], g_touch[g_slot * 5 + 2]);
      else
        g_touch[g_slot * 5 + 3] = -1;
    break;
    }
  }

  if (event->type == EV_SYN) {
    for (unsigned int i = 0; i < sizeof(g_touch) / sizeof(int); i += 5) {
      if (g_touch[i + 4] != 0)
        touch_down(i / 5, g_touch[i + 1], g_touch[i + 2]);
    }
  }
}
