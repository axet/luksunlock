void kbd_init();
int kbd_caps();
void kbd_handle(struct input_event *event);
char kbd_key(struct input_event *event);
