#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <thread>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <vector>
#include <time.h>
#include <sys/wait.h>
#include <linux/input.h>

#include "luksunlock.h"
#include "security.h"
#include "minui/minui.h"
#include "kbd.h"
#include "touch.h"

#ifdef NFC
#include "nfc.h"
#endif

const char* CRYPTSETUP = 0; // /system/xbin/cryptsetup
const char* CRYPTOPTIONS = 0; // --allow-discard
const char* MKFS = 0; // /system/sbin/mkfs.ext4
const char* REBOOT = 0; // /system/sbin/reboot

#define KEY_RELEASE 0
#define KEY_PRESS 1
#define KEY_KEEPING_PRESSED 2

struct Keymap g_keys[CHARS_COUNT];
struct Keymap g_buttons[BUTTONS_MAX];
int g_buttons_count = 0;
char g_passphrase[1024] = {0};
char g_passchange[1024] = {0};
bool g_passflag = 0; // 1 - password changing
std::vector<Pair> g_pairs;
GRSurface* g_background;
int g_current = 0; // currently selected key or button
int g_tab_butt = CHARS_COUNT, tab_key = 0;
int g_fps = 0;
const GRFont *g_font = 0;

const char *escape_input(const char *str) {
  int i, j;
  char *nn = (char*)malloc(strlen(str) * 2 + 1);
  for (i = 0, j = 0; i < (int) strlen(str); i++) {
    if (!(((str[i] >= 'A') && (str[i] <= 'Z')) || ((str[i] >= 'a') && (str[i] <= 'z')) || ((str[i] >= '0') && (str[i] <= '9')) )) {
      nn[j] = '\\';
      j++;
    }
    nn[j] = str[i];
    j++;
  }
  nn[j] = '\0';
  return nn;
}

void gr_text(int x, int y, const char *s) {
  gr_text(g_font, x, y, s, false);
}

void draw_keymap() {
  char keybuf[2];
  for(int i = 0; i < CHARS_COUNT; i++) {
    sprintf(keybuf, "%c", g_keys[i].key);
    if (i == g_current) {
      gr_color(255, 0, 0, 255);
      gr_fill(g_keys[i].xpos, g_keys[i].ypos, g_keys[i].xxpos, g_keys[i].yypos);
      gr_color(255, 255, 255, 255);
    } else {
      gr_color(0, 0, 0, 255);
    }
    gr_text(g_keys[i].xpos, g_keys[i].ypos, keybuf);
  }
}

void draw_buttons() {
  int bindex = g_current - CHARS_COUNT;
  for(int i = 0; i < g_buttons_count; i++) {
    if (i == bindex) {
      gr_color(255, 0, 0, 255);
      gr_fill(g_buttons[i].xpos, g_buttons[i].ypos, g_buttons[i].xxpos, g_buttons[i].yypos);
      gr_color(255, 255, 255, 255);
    } else {
      gr_color(0, 0, 0, 255);
    }
    const char* text = 0;
    switch(g_buttons[i].key) {
    case '>':
      if (g_passflag == 1)
        text = "Confirm";
      else
        text = "Unlock";
    break;
    case '<':
      text = "Delete";
    break;
    case '*':
      text = "Change passphrase";
    break;
    case 'r':
      text = "Reboot";
    break;
    }
    gr_text(g_buttons[i].xpos, g_buttons[i].ypos, text);
  }
}

void ui_init(void) {
  gr_init();
  res_create_display_surface("padlock", &g_background);
  g_font = gr_sys_font();
}

int font_places() {
  return gr_fb_width() / g_font->char_width;
}

int keymap_cols() {
  return font_places() / 3;
}

int keymap_places() {
  return font_places() - 2;
}

void draw_pass() {
  int i, cols = keymap_places();
  int xoff = (gr_fb_width() - cols *  g_font->char_width) / 2;
  for(i = 0; i < (int) strlen(g_passphrase); i++)
    gr_text(xoff + i * g_font->char_width, g_font->char_height * 2, "*");
  for(; i < cols; i++)
    gr_text(xoff + i * g_font->char_width, g_font->char_height * 2, "_");
}

void draw_fps() {
  char buf[64];
  snprintf(buf, sizeof(buf), "%d", g_fps);
  int xpos = gr_fb_width() - g_font->char_width*strlen(buf);
  gr_color(255, 255, 255, 255);
  gr_fill(xpos, 0, gr_fb_width(), g_font->char_height);
  gr_color(0, 0, 0, 255);
  gr_text(xpos, 0, buf);
}

void draw_screen() {
  gr_color(255, 255, 255, 255);
  gr_fill(0, 0, gr_fb_width(), gr_fb_height());

  int xoff = g_font->char_width;

  gr_color(0, 0, 0, 255);
  if (g_passflag == 1)
    gr_text(xoff, g_font->char_height, "Enter NEW phrase: ");
  else
    gr_text(xoff, g_font->char_height, "Enter unlock phrase: ");

  draw_pass();
  draw_keymap();
  draw_buttons();

  gr_color(0, 0, 0, 255);
  gr_text(xoff, gr_fb_height() - g_font->char_height*3, "Press Power to select");
  gr_text(xoff, gr_fb_height() - g_font->char_height*2, "Press VolUp to move left");
  gr_text(xoff, gr_fb_height() - g_font->char_height, "Press VolDown to move right");

  gr_flip();
}

void generate_keymap() {
  int xpos, ypos;
  char key;
  int i;

  int xoff = (gr_fb_width() - keymap_places() * g_font->char_width) / 2;

  xpos = xoff;
  ypos = g_font->char_height * 4;

  for (i = 0, key = CHAR_START; key < CHAR_END; key++, i++, xpos += (g_font->char_width * 3)) {
    if (xpos >= gr_fb_width() - g_font->char_width) {
      ypos += g_font->char_height;
      xpos = xoff;
    }
    g_keys[i].key = key;
    g_keys[i].xpos = xpos;
    g_keys[i].xxpos = xpos + g_font->char_width;
    g_keys[i].ypos = ypos;
    g_keys[i].yypos = ypos + g_font->char_height;
  }

  xpos = g_font->char_width;
  ypos += g_font->char_height;

  g_buttons_count = 0;

  ypos += g_font->char_height;
  g_buttons[g_buttons_count].key = '<';
  g_buttons[g_buttons_count].xpos = xpos;
  g_buttons[g_buttons_count].xxpos = gr_fb_width() - xpos;
  g_buttons[g_buttons_count].ypos = ypos;
  g_buttons[g_buttons_count].yypos = ypos + g_font->char_height;
  g_buttons_count++;

  ypos += g_font->char_height;
  g_buttons[g_buttons_count].key = '>';
  g_buttons[g_buttons_count].xpos = xpos;
  g_buttons[g_buttons_count].xxpos = gr_fb_width() - xpos;
  g_buttons[g_buttons_count].ypos = ypos;
  g_buttons[g_buttons_count].yypos = ypos + g_font->char_height;
  g_buttons_count++;

  if (g_passflag == 0) {
    ypos += g_font->char_height;
    g_buttons[g_buttons_count].key = '*';
    g_buttons[g_buttons_count].xpos = xpos;
    g_buttons[g_buttons_count].xxpos = gr_fb_width() - xpos;
    g_buttons[g_buttons_count].ypos = ypos;
    g_buttons[g_buttons_count].yypos = ypos + g_font->char_height;
    g_buttons_count++;
  }

  ypos += g_font->char_height;
  g_buttons[g_buttons_count].key = 'r';
  g_buttons[g_buttons_count].xpos = xpos;
  g_buttons[g_buttons_count].xxpos = gr_fb_width() - xpos;
  g_buttons[g_buttons_count].ypos = ypos;
  g_buttons[g_buttons_count].yypos = ypos + g_font->char_height;
  g_buttons_count++;
}

void gr_msg(const char* msg) {
  gr_color(0, 0, 0, 255);
  gr_fill(0, 0, gr_fb_width(), gr_fb_height());
  gr_color(255, 255, 255, 255);
  gr_text((gr_fb_width() / 2) - ((strlen(msg) / 2) * g_font->char_width), gr_fb_height() / 2, msg);
  gr_flip();
}

bool unlock(const char *dev, const char *map) {
  char buffer[4096];
  int fd, failed = 0;
  int format = 0;

  fd = open(dev, 0);
  if (fd > 0) {
    int r = read(fd, buffer, sizeof(buffer));
    if (r > 0 && memcmp(buffer, "LUKS", 4) != 0) {
      gr_msg("Encrypting...");
      snprintf(buffer, sizeof(buffer), "echo %s | %s luksFormat %s", escape_input(g_passphrase), CRYPTSETUP, dev);
      system(buffer);
      format = 1; // we have to format new encypted drive with ext4
    }
    close(fd);
  }

  if (g_passflag == 1) {
    gr_msg("Changing key...");
    const char *str = "{ echo \"%s\"; echo \"%s\"; } | %s luksChangeKey %s";
    snprintf(buffer, sizeof(buffer), str, escape_input(g_passchange), escape_input(g_passphrase), CRYPTSETUP, dev);
    system(buffer);
    g_passflag = 0;
  }

  gr_msg("Unlocking...");

  snprintf(buffer, sizeof(buffer), "echo %s | %s %s luksOpen %s %s", escape_input(g_passphrase), CRYPTSETUP, CRYPTOPTIONS, dev, map);
  system(buffer);

  snprintf(buffer, sizeof(buffer), "/dev/mapper/%s", map);
  fd = open(buffer, 0);
  if (fd < 0)
    failed = 1;
  else
    close(fd);

  if (!failed) {
    if (format == 1 && MKFS != 0) {
      gr_msg("Formatting...");
      snprintf(buffer, sizeof(buffer), "%s /dev/mapper/%s", MKFS, map);
      system(buffer);
    }
    gr_msg("Success!");
    return true;
  }

  gr_msg("Failed!");

  return false;
}

void key_unlock() {
  bool b = true;
  for (auto &p : g_pairs)
    b &= unlock(p.dev, p.map);
  if (b)
    exit(0);
  sleep(2);
  g_passphrase[0] = '\0';
  g_current = 0;
  draw_screen();
}

void key_del() {
  int pos = strlen(g_passphrase) - 1;
  if (pos < 0)
    pos = 0;
  g_passphrase[pos] = '\0';
}

void key_select() {
  int bindex = g_current - CHARS_COUNT;
  if (bindex >= 0) {
    switch(g_buttons[bindex].key) {
    case '>':
      key_unlock();
    break;
    case '<':
      key_del();
    break;
    case '*':
      strncpy(g_passchange, g_passphrase, sizeof(g_passphrase));
      g_passflag = 1;
      g_passphrase[0] = '\0';
      g_current = 0;
    break;
    case 'r':
      system(REBOOT);
    break;
    }
    generate_keymap();
  } else {
    snprintf(g_passphrase, sizeof(g_passphrase), "%s%c", g_passphrase, g_keys[g_current].key);
  }
}

void key_left() {
  if (g_current > 0)
    g_current--;
  else
    g_current = CHARS_COUNT - 1 + g_buttons_count;
}

void key_right() {
  if (g_current < CHARS_COUNT - 1 + g_buttons_count)
    g_current++;
  else
    g_current = 0;
}

void key_up() {
  int cols = keymap_cols();
  if (g_current - CHARS_COUNT > 0)
    g_current--;
  else if (g_current - CHARS_COUNT >= 0)
    g_current = CHARS_COUNT/cols*cols;
  else if (g_current - cols >= 0)
    g_current -= cols;
}

void key_down() {
  int cols = keymap_cols();
  if (g_current + cols < CHARS_COUNT)
    g_current += cols;
  else if (g_current - CHARS_COUNT < 0)
    g_current = CHARS_COUNT;
  else if (g_current - CHARS_COUNT - g_buttons_count < -1)
    g_current++;
}

void handle_key(struct input_event *event) { // use 'getevent -l'
  if (event->type == EV_REL) {
    if (event->code == 1) {
      if(event->value > 0) {
        key_up();
      } else if(event->value < 0) {
        key_down();
      }
    } else if (event->code == 0) {
      if(event->value > 0)
        key_right();
      else if(event->value < 0)
        key_left();
    }
  }

  if ( event->type == EV_KEY && ( event->value == KEY_PRESS || event->value == KEY_KEEPING_PRESSED) ) {
    switch(event->code) {
    case KEY_LEFT:
    case KEY_VOLUMEUP:
      key_left();
    break;
    case KEY_RIGHT:
    case KEY_VOLUMEDOWN:
      key_right();
    break;
    case KEY_UP:
      key_up();
    break;
    case KEY_DOWN:
      key_down();
    break;
    case KEY_TAB:
      if (g_current >= CHARS_COUNT) {
        g_tab_butt = g_current;
        g_current = tab_key;
      } else {
        tab_key = g_current;
        g_current = g_tab_butt;
      }
    break;
    case KEY_BACKSPACE:
    case BTN_RIGHT:
      key_del();
    break;
    case KEY_KPENTER:
    case KEY_ENTER:
      key_unlock();
    break;
    case BTN_MOUSE:
    case KEY_POWER:
      key_select();
    break;
    default:
      char k = kbd_key(event);
      for (int i = 0; i < CHARS_COUNT; i++) {
        if (g_keys[i].key == k) {
          g_current = i;
          snprintf(g_passphrase, sizeof(g_passphrase), "%s%c", g_passphrase, k);
          break;
        }
      }
    }
  }
}

static int input_cb(int fd, uint32_t epevents) {
  struct input_event ev;
  if (ev_get_input(fd, epevents, &ev) == -1)
    return -1;
  kbd_handle(&ev);
  handle_touch(&ev);
  handle_key(&ev);
  draw_screen();
  return 0;
}

const char* findbin(const char *bin, ...) {
  va_list args;
  va_start(args, bin);
  char buf[1024];
  const char* CC[] = { "/system/xbin", "/system/sbin", "/system/bin", "/sbin", "/bin", NULL };
  for (const char *str = bin; str != NULL; str = va_arg(args, const char *)) {
    for (int i=0; CC[i]!=0; i++) {
      snprintf(buf, sizeof(buf), "%s/%s", CC[i], str);
      if (access(buf, F_OK) == 0)
        return strdup(buf); // free()
    }
  }
  return 0;
}

long time_ms() {
  timespec tp;
  clock_gettime(CLOCK_MONOTONIC, &tp);
  return tp.tv_sec * 1000 + tp.tv_nsec / 1000000;
}

void* autolog(void* v) {
  while (v) {
    fflush(stdout);
    fflush(stderr);
    sleep(1);
  }
  int kmsg = open("/dev/kmsg", O_WRONLY | O_APPEND);
  if (kmsg != 0) {
    dup2(kmsg, fileno(stdout));
    dup2(kmsg, fileno(stderr));
    pthread_t tid;
    pthread_create(&tid, NULL, &autolog, (void*)1);
  }
  return 0;
}

void* autologin(void* sec){
  if (sec == 0) {
    pthread_t tid;
    pthread_create(&tid, NULL, &autologin, (void*)20);
    return 0;
  }
  sleep((int)sec);
  key_unlock();
  return 0;
}

int luksunlock(int argc, char **argv) {
  autolog(0);
  int k = 1;
  std::string opts;
  for (; k < argc; k++) {
    if (argv[k][0] == '-')
      opts += argv[k];
    else
      break;
    opts += " ";
  }
  CRYPTOPTIONS = strdup(opts.c_str());
  for (; k < argc; k += 2)
    g_pairs.push_back(Pair(argv[k], argv[k+1]));

  CRYPTSETUP = findbin("cryptsetup", NULL);
  MKFS = findbin("mkfs.ext4", "make_ext4fs", NULL);
  REBOOT = findbin("reboot", NULL);

#ifdef NFC
  nfc_init();
#endif

  kbd_init();
  ui_init();
  generate_keymap();
  draw_screen();

  ev_init(std::bind(input_cb, std::placeholders::_1, std::placeholders::_2), true);

  int count = 0;
  long last = time_ms();
  for(;;) {
    long now = time_ms();
    long diff = now - last;
    if (diff > 1000) {
      last = now;
      g_fps = count * 1000 / diff;
      count = 0;
    }
    if (ev_wait(500) == 0)
      ev_dispatch();
#ifdef NFC
    char buf[1024];
    if (nfc_read(buf, sizeof(buf)) > 0) {
      snprintf(g_passphrase, sizeof(g_passphrase), "%s%s", g_passphrase, buf);
      draw_screen();
      key_unlock();
    }
#endif
    count++;
  }

  return 0;
}

int main(int argc, char **argv, char **envp) {
  int w;
  do {
    pid_t pid = fork();
    if (pid == 0) {
      return luksunlock(argc, argv);
    } else {
      waitpid(pid, &w, 0);
    }
  } while (w != 0);
}
