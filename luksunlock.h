#define CHAR_START    0x20
#define CHAR_END      0x7E
#define CHARS_COUNT   (CHAR_END - CHAR_START)
#define BUTTONS_MAX   10

struct Pair {
  const char *dev;
  const char *map;
  Pair(const char *d, const char*m):dev(d), map(m) {}
};

struct Keymap {
  unsigned char key;
  int xpos;
  int xxpos;
  int ypos;
  int yypos;
};

const char *escape_input(const char *str);
void key_select();

extern const char* CRYPTSETUP;
extern struct Keymap g_keys[CHARS_COUNT];
extern int g_buttons_count;
extern int g_current;
extern char g_passphrase[1024];
extern std::vector<Pair> g_pairs;
