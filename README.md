# luksunlock

Simple android shared/static binary which unlock luks layer.

To install luksunlock you need add it to the /init.rc:

```
    mount_all ./fstab --early
    exec u:r:cryptsetup:s0 root root -- /system/xbin/luksunlock /dev/block/platform/msm_sdcc.1/by-name/userdata luks
    mount_all ./fstab --late
```

And replace old data entires in /etc/fstab:

```
/dev/block/platform/msm_sdcc.1/by-name/userdata /data ext4 noatime,nosuid,nodev wait,check
```

with:

```
/dev/mapper/luks  /data  ext4  noatime,nosuid,nodev latemount,wait,check
```

# Features

* touchscreen
* usb keyboards
* nfc

# Links

* https://forum.xda-developers.com/t/minimal-ui-for-luks-encryption-on-the-wildfire.866131
