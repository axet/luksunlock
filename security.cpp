#include <vector>
#include <unistd.h>
#include <utime.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "luksunlock.h"
#include "security.h"

void killswitch(const char* pass) {
  if (strcmp(g_passphrase, pass) == 0) {
    for (auto &p : g_pairs) {
      char buffer[1024];
      snprintf(buffer, sizeof(buffer), "echo %s | %s luksFormat %s", escape_input(g_passphrase), CRYPTSETUP, p.dev);
      system(buffer);
    }
  }
}

void deadline(bool daemon, int d) {
  const char *f = "/persist/luksunlock";
  if (daemon) {
    int pid = fork();
    while (pid == 0) {
      close(open(f, O_CREAT | S_IRUSR | S_IWUSR, S_IRWXU));
      utime(f, 0);
      sleep(6 * 60 * 60);
    }
  } else {
    struct stat fs;
    if ( stat(f, &fs) == 0 ) {
      struct timespec now;
      clock_gettime(CLOCK_REALTIME, &now);
      if (fs.st_mtim.tv_sec + d < now.tv_sec || now.tv_sec < fs.st_mtim.tv_sec) {
        const char *str = "echo \"%s\" | %s luksRemoveKey %s";
        for (auto &p : g_pairs) {
          char buffer[1024];
          snprintf(buffer, sizeof(buffer), str, escape_input(g_passphrase), CRYPTSETUP, p.dev);
          system(buffer);
        }
      }
    }
  }
}
