#include <linux/kd.h>
#include <sys/ioctl.h>
#include <ctype.h>
#include <linux/input.h>
#include <linux/vt.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

static int nums = 0;
static int caps = 0;
static int lshift = 0;
static int rshift = 0;
static char map[2048] = {0};
static char keys[] = {
  KEY_Q,          'q', 'Q',
  KEY_W,          'w', 'W',
  KEY_E,          'e', 'E',
  KEY_R,          'r', 'R',
  KEY_T,          't', 'T',
  KEY_Y,          'y', 'Y',
  KEY_U,          'u', 'U',
  KEY_I,          'i', 'I',
  KEY_O,          'o', 'O',
  KEY_P,          'p', 'P',
  KEY_LEFTBRACE,  '[', '{',
  KEY_RIGHTBRACE, ']', '}',
  KEY_BACKSLASH,  '\\', '|',
  KEY_1,          '1', '!',
  KEY_2,          '2', '@',
  KEY_3,          '3', '#',
  KEY_4,          '4', '$',
  KEY_5,          '5', '%',
  KEY_6,          '6', '^',
  KEY_7,          '7', '&',
  KEY_8,          '8', '*',
  KEY_9,          '9', '(',
  KEY_0,          '0', ')',
  KEY_MINUS,      '-', '_',
  KEY_EQUAL,      '=', '+',
  KEY_GRAVE,      '`', '~',
  KEY_A,          'a', 'A',
  KEY_S,          's', 'S',
  KEY_D,          'd', 'D',
  KEY_F,          'f', 'F',
  KEY_G,          'g', 'G',
  KEY_H,          'h', 'H',
  KEY_J,          'j', 'J',
  KEY_K,          'k', 'K',
  KEY_L,          'l', 'L',
  KEY_SEMICOLON,  ';', ':',
  KEY_APOSTROPHE, '\'', '\"',
  KEY_Z,          'z', 'Z',
  KEY_X,          'x', 'X',
  KEY_C,          'c', 'C',
  KEY_V,          'v', 'V',
  KEY_B,          'b', 'B',
  KEY_N,          'n', 'N',
  KEY_M,          'm', 'M',
  KEY_COMMA,      ',', '<',
  KEY_DOT,        '.', '>',
  KEY_SLASH,      '/', '?',
  KEY_SPACE,      ' ', ' ',
  KEY_KP1,          0, '1',
  KEY_KP2,          0, '2',
  KEY_KP3,          0, '3',
  KEY_KP4,          0, '4',
  KEY_KP5,          0, '5',
  KEY_KP6,          0, '6',
  KEY_KP7,          0, '7',
  KEY_KP8,          0, '8',
  KEY_KP9,          0, '9',
  KEY_KP0,          0, '0',
  KEY_KPDOT,        0, '.',
  KEY_KPSLASH,    '/', '/',
  KEY_KPMINUS,    '-', '-',
  KEY_KPPLUS,     '+', '+',
  KEY_KPASTERISK, '*', '*',
};

void kbd_init() {
  for (unsigned int i = 0; i < sizeof(keys); i += 3) {
    int code = keys[i];
    map[code * 2] = keys[i + 1];
    map[code * 2 + 1] = keys[i + 2];
  }
}

void kbd_caps() { // read kernel caps / nums states
  int fd;
  vt_stat stat;
  fd = open("/dev/tty0", O_RDONLY);
  if (ioctl(fd, VT_GETSTATE, &stat) == -1)
    return;
  close(fd);
  char tty[128];
  sprintf(tty, "/dev/tty%d", stat.v_active);
  char result;
  fd = open(tty, O_RDWR | O_NDELAY, 0);
  if (ioctl(fd, KDGETLED, &result) == -1)
    return;
  close(fd);
  caps = (result & LED_CAP) == LED_CAP;
  nums = (result & LED_NUM) == LED_NUM;
}

void kbd_handle(struct input_event *event) {
  if (event->type == EV_LED) {
    switch(event->code) {
    case LED_CAPSL:
      caps = event->value;
    break;
    case LED_NUML:
      nums = event->value;
    break;
    }
  }
  if (event->type == EV_KEY) {
    switch(event->code) {
    case KEY_LEFTSHIFT:
      lshift = event->value != 0;
    break;
    case KEY_RIGHTSHIFT:
      rshift = event->value != 0;
    break;
    }
  }
}

char kbd_key(struct input_event *event) {
  int index;
  switch (event->code) {
  case KEY_KP1:
  case KEY_KP2:
  case KEY_KP3:
  case KEY_KP4:
  case KEY_KP5:
  case KEY_KP6:
  case KEY_KP7:
  case KEY_KP8:
  case KEY_KP9:
  case KEY_KP0:
  case KEY_KPDOT:
    index = nums == 1;
  break;
  default:
    index = caps == 1;
    if ( lshift == 1 || rshift == 1 )
      index = !index;
  break;
  }
  return map[event->code * 2 + index];
}
